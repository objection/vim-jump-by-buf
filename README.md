# Jump By Buf

In Vim, <c-o> and <c-i> let you go backwards and forwards through the
jumplist. This plugin lets you go back through the jumplist by buffer.

## Explanation

Here's a jumplist, as it's rendered when you type :jumps.

```
 jump line  col file/text
  7     68   22 ~/.vim/plugged/vim-capslock/plugin/capslock.vim
  6     33    0 ~/git/set-up/dotfiles/vim/plugins/plugins.vim
  5    120   10 ~/.vimrc
  4      1   10 ~/.vimrc
  3     56   10 ~/.vimrc
  2     43   10 ~/.vimrc
  1    200   24 ~/.vimrc
>
```

You're at the >. You're off the end of the jumplist. If you type
whatever you bind jump-backwards to, you'll end up in ~/.vimrc, just
as you would if you'd typed <c-o>. However, when you type it again,
you'll end up in ~/git/set-up/dotfiles/vim/plugins/plugins.vim,
skipping all of the other jumps in ~/.vimrc.

## Mappings

I haven't bound any maps for you. Set them like this:

```
nmap <a-o> <Plug>(jump_by_buf_backwards)
nmap <a-i> <Plug>(jump_by_buf_backwards)
```

I've used <a-{o,i}>, betraying that fact I use gvim. How about
g<c-{o,i}>? Leader<c-{o,i}>?
