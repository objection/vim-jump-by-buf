if &cp || exists ("g:loaded_jump_by_buf")
  finish
endif
let g:loaded_jump_by_buf = 1

let s:cpo_save = &cpo
set cpo&vim

func s:warn (str)
	if exists ('g:jump_by_buf_no_warnings') && g:jump_by_buf_no_warnings == 1
		return 1
	endif
	echom "jump-by-buf:" a:str
endfunc

func s:jump_back ()
	normal 
endfunc

func s:jump_forwards ()
	" This is actually normal + <c-i> AKA tab. For some reason
	" I cant get exe "normal! \<c-i>" to work. Hopefully your
	" get-rid-of-whitespace autocmd won't get rid of this. Mine
	" doesn't.
	normal 1 	
endfunc

func! s:jump_by_buf (direction, count) abort
	let jump_list_dict = getjumplist ()

	let jump_list = jump_list_dict[0]
	if len (jump_list) == 0
		call s:warn ("Only one buffer in jumplist")
		return 1
	endif
	let current_pos = jump_list_dict[1]
	let starting_bufnr = bufnr ()

	let i = current_pos
	let this_bufnr = starting_bufnr


	let Jump_func = a:direction ==# "b"
				\ ? funcref ('s:jump_back')
				\ : funcref ('s:jump_forwards')
	let end_bufnr = a:direction ==# "b"
				\ ? jump_list[0].bufnr
				\ : jump_list[len (jump_list) - 1].bufnr

	" If you just do the mapping you'll get a count on 0, which does
	" nothing. Hence, I'll make it one in that case. I don't know if
	" this is the way people do it. This means if you explicitly did
	" 0<mapping> you'd still move. Obviously no one will ever care
	" about this, since no one (right?) would ever do that.
	for i in range (a:count == 0? 1: a:count)

		" It we're already the first or last buf in the jumplist, we stop.
		if this_bufnr == end_bufnr
			" Don't use echoerr, it's too over-the-top. All we want is a reminder, not
			" a heart attack.
			call s:warn ("Can't jump any more in this direction")
			return 1
		endif
		" We just run <c-o> or <c-i> repeatedly until the bufnr changes.
		while this_bufnr == starting_bufnr
			call Jump_func ()
			let this_bufnr = bufnr ()
		endwhile

		let starting_bufnr = bufnr ()
	endfor
	return 0
endfunc

" nnoremap <silent> <Plug>(dirvish_up) :<C-U>exe 'Dirvish %:p'.repeat(':h',v:count1)<CR>
" nmap - <Plug>(dirvish_up)
" nnoremap <silent> <Plug>CapsLockToggle  :<C-U>call <SID>toggle('i',1)<CR>
nnoremap <silent> <Plug>(jump_by_buf_backwards) <Cmd>call <SID>jump_by_buf ('b', v:count)<CR>
nnoremap <silent> <Plug>(jump_by_buf_forwards) <Cmd>call <SID>jump_by_buf ('f', v:count)<CR>

let &cpo = s:cpo_save

